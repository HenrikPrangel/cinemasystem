﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class MovieInCinema
    {
        public int MovieInCinemaId { get; set; }

        //From and Until should be reworked to allow rescreenings
        public DateTime InCinemaFrom { get; set; }
        public DateTime InCinemaUntil { get; set; }


        public int CinemaId { get; set; }
        public Cinema Cinema { get; set; }

        public int MovieId { get; set; }
        public Movie Movie { get; set; }

        public virtual List<Session> Sessions { get; set; } = new List<Session>();



    }
}
