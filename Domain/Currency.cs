﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Currency
    {
        public int CurrencyId { get; set; }

        [Required]
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Required]
        [MaxLength(50)]
        public string CurrencyName { get; set; }
        public decimal ExchangeRate { get; set; } //Relative to the Euro


        public virtual List<Price> Prices { get; set; } = new List<Price>();

    }
}
