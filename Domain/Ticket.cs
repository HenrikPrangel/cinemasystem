﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Ticket
    {
        public int TicketId { get; set; }

        [MaxLength(100)]
        public string TicketCinemaName { get; set; }
        [MaxLength(50)]
        public string TicketMovieName { get; set; }
        public DateTime TicketMovieDate { get; set; }
        public int TicketRow { get; set; }
        public int TicketSeat { get; set; }
        public int TicketMovieAgeRestriction { get; set; }
        public decimal TicketPrice { get; set; }
        public TicketHolder ?TicketHolder { get; set; }


        public int SessionId { get; set; }
        public Session Session { get; set; }

        public int SeatId { get; set; }
        public Seat Seat { get; set; }

        public int OrderRowId { get; set; }
        public OrderRow OrderRow { get; set; }

        public virtual List<Price> TicketPrices { get; set; } = new List<Price>();
        public virtual List<Discount> TicketDiscounts { get; set; } = new List<Discount>();
    }
}
