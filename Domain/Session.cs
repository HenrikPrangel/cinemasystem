﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Session
    {
        public int SessionId { get; set; }

        //public DateTime SessionDay { get; set; }
        public DateTime SessionBegins { get; set; }
        public DateTime SessionEnds { get; set; }


        public int MovieInCinemaId { get; set; }
        public MovieInCinema MovieInCinema { get; set; }

        public int CinemaHallId { get; set; }
        public CinemaHall CinemaHall { get; set; }

        public virtual List<Ticket> SessionTickets { get; set; } = new List<Ticket>();

    }
}
