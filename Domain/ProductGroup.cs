﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class ProductGroup
    {

        public int ProductGroupId { get; set; }

        //ProductGroup recursion?
        [Required]
        [MaxLength(50)]
        public string ProductGroupName { get; set; }
        [MaxLength(200)]
        public string ProductGroupDescription { get; set; }

        public virtual List<Product> Products { get; set; } = new List<Product>();
        public virtual List<Discount> Discounts { get; set; } = new List<Discount>();


    }
}
