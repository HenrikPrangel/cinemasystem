﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Price
    {

        public int PriceId { get; set; }

        public decimal PriceValue { get; set; }


        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }

        public int ?TicketId { get; set; }
        public Ticket Ticket { get; set; }

        public int ?CampaignId { get; set; }
        public Campaign Campaign { get; set; }

        public int ?ProductId { get; set; }
        public Product Product { get; set; }

    }
}
