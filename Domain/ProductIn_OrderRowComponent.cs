﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class ProductIn_OrderRowComponent
    {
        public int ProductIn_OrderRowComponentId { get; set; }


        public int ProductQuantity { get; set; }
        public decimal ProductInOrderRowComponent_Total { get; set; }


        public int OrderRowComponentId { get; set; }
        public OrderRowComponent OrderRowComponent { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
