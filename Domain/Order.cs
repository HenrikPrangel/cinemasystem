﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Order
    {

        public int OrderId { get; set; }

        public decimal OrderTotal { get; set; }

        //Nullable?
        public int UserId { get; set; }
        public User User { get; set; }

        public virtual List<OrderRow> OrderRows { get; set; } = new List<OrderRow>();
        public virtual List<Payment> Payments { get; set; } = new List<Payment>();
        public virtual List<Discount> Discounts { get; set; } = new List<Discount>();


    }
}
