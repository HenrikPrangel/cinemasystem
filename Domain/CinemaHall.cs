﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class CinemaHall
    {
        public int CinemaHallId { get; set; }

        [Required]
        [MaxLength(10)]
        public string CinemaHallName { get; set; }
        public int SeatCount { get; set; }


        public int CinemaId { get; set; }
        public Cinema Cinema { get; set; }

        public virtual List<Session> SessionsInCinemaHall { get; set; } = new List<Session>();
        public virtual List<Row> RowsInCinemaHall { get; set; } = new List<Row>();


    }
}
