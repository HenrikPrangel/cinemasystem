﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Cinema
    {
        public int CinemaId { get; set; }

        [Required]
        [MaxLength(50)]
        public string CinemaName { get; set; }
        [Required]
        [MaxLength(100)]
        public string Address { get; set; }


        public virtual List<MovieInCinema> MoviesInCinema { get; set; } = new List<MovieInCinema>();

        public virtual List<CinemaHall> CinemaHalls { get; set; } = new List<CinemaHall>();

        public virtual List<Info> CinemaInfoList { get; set; } = new List<Info>();

    }
}
