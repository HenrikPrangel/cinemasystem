﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class InfoType
    {
        public int InfoTypeId { get; set; }

        [Required]
        [MaxLength(20)]
        public string InfoTypeName { get; set; }
        [MaxLength(50)]
        public string InfoTypeDescription { get; set; }

        //InfoType recursion?
        public virtual List<Info> InfoList { get; set; } = new List<Info>();


    }
}
