﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class OrderRow
    {

        public int OrderRowId { get; set; }


        public int Quantity { get; set; }
        public decimal OtherPriceTotal { get; set; }
        public decimal TicketPriceTotal { get; set; }
        public decimal OrderRowTotalPrice { get; set; }


        public int OrderId { get; set; }
        public Order Order { get; set; }


        public virtual List<Ticket> OrderRowTickets { get; set; } = new List<Ticket>();
        public virtual List<OrderRowComponent> OrderRowComponents { get; set; } = new List<OrderRowComponent>();

    }
}
