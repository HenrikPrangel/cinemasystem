﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Row
    {
        public int RowId { get; set; }

        public int RowNumber { get; set; }
        public int SeatAmount { get; set; }


        public int CinemaHallId { get; set; }
        public CinemaHall CinemaHall { get; set; }

        public virtual List<Seat> Seats { get; set; } = new List<Seat>();



    }
}
