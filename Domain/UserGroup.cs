﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class UserGroup
    {
        public int UserGroupId { get; set; }

        [Required]
        [MaxLength(50)]
        public string UserGroupName { get; set; }


        public virtual List<Discount> UserGroupDiscounts { get; set; } = new List<Discount>();
        public virtual List<User> Users { get; set; } = new List<User>();
        public virtual List<Info> UserGroupInfo { get; set; } = new List<Info>();


    }
}
