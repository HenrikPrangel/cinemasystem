﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Seat
    {
        public int SeatId { get; set; }

        public int SeatNumber { get; set; }


        public int RowId { get; set; }
        public Row Row { get; set; }

        public virtual List<Ticket> SeatTickets { get; set; } = new List<Ticket>();

    }
}
