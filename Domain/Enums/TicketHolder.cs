﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum TicketHolder
    {
        Student,
        Pensioner,
        Child,
        NormalUser,
        ClubFan
    }
}
