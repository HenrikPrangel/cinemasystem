﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class OrderRowComponent
    {
        public int OrderRowComponentId { get; set; }

        [Required]
        [MaxLength(20)]
        public string Description_OrderRowComponent { get; set; }
        public decimal OrderRowComponentTotal { get; set; }

        public int OrderRowId { get; set; }
        public OrderRow OrderRow { get; set; }

        public virtual List<ProductIn_OrderRowComponent> ProductsIn_OrderRowComponent { get; set; } = new List<ProductIn_OrderRowComponent>();

    }
}
