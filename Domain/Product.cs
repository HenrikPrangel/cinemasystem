﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Product
    {
        public int ProductId { get; set; }

        [Required]
        [MaxLength(50)]
        public string ProductName { get; set; }


        public int ProductGroupId { get; set; }
        public ProductGroup ProductGroup { get; set; }

        public virtual List<Info> ProductInfoList { get; set; } = new List<Info>();
        public virtual List<ProductIn_OrderRowComponent> ProductsIn_OrderRowComponent { get; set; } = new List<ProductIn_OrderRowComponent>();
        public virtual List<Discount> Discounts { get; set; } = new List<Discount>();
        public virtual List<Price> Prices { get; set; } = new List<Price>();


    }
}
