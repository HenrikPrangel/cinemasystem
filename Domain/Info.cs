﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    /// <summary>
    /// This class handles information storage, that needs to be
    /// available many different languages.
    /// (class exist so webpage could be multilingual)
    /// </summary>
    public class Info
    {
        public int InfoId { get; set; }


        //TODO: Annotation should be assigned by InformationType
        [Required]
        [MaxLength(500)]
        public string Description { get; set; }


        public int InfoTypeId { get; set; }
        public InfoType InfoType { get; set; }

        public int ?CinemaId { get; set; }
        public Cinema Cinema { get; set; }

        public int ?MovieId { get; set; }
        public Movie Movie { get; set; }

        public int ?ProductId { get; set; }
        public Product Product { get; set; }

        public int ?CampaignId { get; set; }
        public Campaign Campaign { get; set; }

        public int ?UserGroupId { get; set; }
        public UserGroup UserGroup { get; set; }

        public int ?DiscountId { get; set; }
        public Discount Discount { get; set; }

        public int ?DiscountTypeId { get; set; }
        public DiscountType DiscountType { get; set; }
    }
}
