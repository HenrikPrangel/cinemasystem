﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// https://www.upload.ee/files/8007143/ContactSolution.zip.html

namespace Domain
{
    public class User
    {
        public int UserId { get; set; }

        [Required]
        [MaxLength(128)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(128)]
        public string LastName { get; set; }
        //Is this how passwords are kept?
        [MaxLength(60)]
        public string Password { get; set; }
        public DateTime JoinDate { get; set; }

        [Required]
        [MaxLength(10)]
        public string LanguagePreferance { get; set; }


        public int UserGroupId { get; set; }
        public UserGroup UserGroup { get; set; }

        public virtual List<Order> UserOrders { get; set; } = new List<Order>();


        public string FirstLastName => $"{FirstName} {LastName}";
        public string LastFirstName => $"{LastName} {FirstName}";
    }
}
