﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Discount
    {
        public int DiscountId { get; set; }

        [Required]
        [MaxLength(50)]
        public string DisccountName { get; set; }
        public decimal DiscountValue { get; set; }


        public int ?CampaignId { get; set; }
        public Campaign Campaign { get; set; }

        public int ?UserGroupId { get; set; }
        public UserGroup UserGroup { get; set; }

        public int ?OrderId { get; set; }
        public Order Order { get; set; }

        public int ?ProductId { get; set; }
        public Product Product { get; set; }

        public int ?ProductGroupId { get; set; }
        public ProductGroup ProductGroup { get; set; }

        public int DiscountTypeId { get; set; }
        public DiscountType DiscountType { get; set; }

        public int ?TicketId { get; set; }
        public Ticket Ticket { get; set; }

        public virtual List<Info> DiscountInfoList { get; set; } = new List<Info>();

    }
}
