﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Movie
    {
        public int MovieId { get; set; }

        [Required]
        [MaxLength(50)]
        public string MovieName { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int AgeRestriction { get; set; }


        public virtual List<Info> MovieInfoList { get; set; } = new List<Info>();
        public virtual List<MovieInCinema> MoviesInCinema { get; set; } = new List<MovieInCinema>();
        //public virtual List<PersonRoleInMovie> PeopleAssociatedWithMovie { get; set; } = new List<PersonRoleInMovie>();


    }
}
