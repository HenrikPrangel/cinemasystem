﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Campaign
    {
        public int CampaignId { get; set; }

        [Required]
        [MaxLength(50)]
        public string CampaignName { get; set; }
        public DateTime CampaignBegins { get; set; }
        public DateTime CampaignEnds { get; set; }


        public virtual List<Info> CampaignInfoList { get; set; } = new List<Info>();
        public virtual List<Price> Prices { get; set; } = new List<Price>();
        public virtual List<Discount> Discounts { get; set; } = new List<Discount>();



    }
}
