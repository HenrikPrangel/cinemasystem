﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class DiscountType
    {
        public int DiscountTypeId { get; set; }

        [Required]
        [MaxLength(50)]
        public string DiscountTypeName { get; set; }


        public virtual List<Discount> DiscountTypeDiscounts { get; set; } = new List<Discount>();
        public virtual List<Info> DiscountTypeInfo { get; set; } = new List<Info>();

    }
}
