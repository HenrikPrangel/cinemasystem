﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class ProductIn_OrderRowComponentController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public ProductIn_OrderRowComponentController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: ProductIn_OrderRowComponent
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.ProductsIn_OrderRowComponents.Include(p => p.OrderRowComponent).Include(p => p.Product);
            return View(await _uow.ProductsIn_OrderRowComponents.AllAsync());
        }

        // GET: ProductIn_OrderRowComponent/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productIn_OrderRowComponent = await _uow.ProductsIn_OrderRowComponents.FindAsync(id);
                //.Include(p => p.OrderRowComponent)
                //.Include(p => p.Product)
                //.SingleOrDefaultAsync(m => m.ProductIn_OrderRowComponentId == id);
            if (productIn_OrderRowComponent == null)
            {
                return NotFound();
            }

            return View(productIn_OrderRowComponent);
        }

        // GET: ProductIn_OrderRowComponent/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["OrderRowComponentId"] = new SelectList(_uow.OrderRowComponents, "OrderRowComponentId", "OrderRowComponentId");
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId");
            ViewData["OrderRowComponentId"] = new SelectList(await _uow.OrderRowComponents.AllAsync(), nameof(OrderRowComponent.OrderRowComponentId), nameof(OrderRowComponent.OrderRowComponentId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));

            return View();
        }

        // POST: ProductIn_OrderRowComponent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductIn_OrderRowComponentId,ProductQuantity,ProductInOrderRowComponent_Total,OrderRowComponentId,ProductId")] ProductIn_OrderRowComponent productIn_OrderRowComponent)
        {
            if (ModelState.IsValid)
            {
                _uow.ProductsIn_OrderRowComponents.Add(productIn_OrderRowComponent);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderRowComponentId"] = new SelectList(_uow.OrderRowComponents, "OrderRowComponentId", "OrderRowComponentId", productIn_OrderRowComponent.OrderRowComponentId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId", productIn_OrderRowComponent.ProductId);
            ViewData["OrderRowComponentId"] = new SelectList(await _uow.OrderRowComponents.AllAsync(), nameof(OrderRowComponent.OrderRowComponentId), nameof(OrderRowComponent.OrderRowComponentId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            return View(productIn_OrderRowComponent);
        }

        // GET: ProductIn_OrderRowComponent/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productIn_OrderRowComponent = await _uow.ProductsIn_OrderRowComponents.FindAsync(id);
            if (productIn_OrderRowComponent == null)
            {
                return NotFound();
            }
            //ViewData["OrderRowComponentId"] = new SelectList(_uow.OrderRowComponents, "OrderRowComponentId", "OrderRowComponentId", productIn_OrderRowComponent.OrderRowComponentId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId", productIn_OrderRowComponent.ProductId);
            ViewData["OrderRowComponentId"] = new SelectList(await _uow.OrderRowComponents.AllAsync(), nameof(OrderRowComponent.OrderRowComponentId), nameof(OrderRowComponent.OrderRowComponentId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            return View(productIn_OrderRowComponent);
        }

        // POST: ProductIn_OrderRowComponent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductIn_OrderRowComponentId,ProductQuantity,ProductInOrderRowComponent_Total,OrderRowComponentId,ProductId")] ProductIn_OrderRowComponent productIn_OrderRowComponent)
        {
            if (id != productIn_OrderRowComponent.ProductIn_OrderRowComponentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.ProductsIn_OrderRowComponents.Update(productIn_OrderRowComponent);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.ProductsIn_OrderRowComponents.Exists(productIn_OrderRowComponent.ProductIn_OrderRowComponentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderRowComponentId"] = new SelectList(_uow.OrderRowComponents, "OrderRowComponentId", "OrderRowComponentId", productIn_OrderRowComponent.OrderRowComponentId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId", productIn_OrderRowComponent.ProductId);
            ViewData["OrderRowComponentId"] = new SelectList(await _uow.OrderRowComponents.AllAsync(), nameof(OrderRowComponent.OrderRowComponentId), nameof(OrderRowComponent.OrderRowComponentId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            return View(productIn_OrderRowComponent);
        }

        // GET: ProductIn_OrderRowComponent/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productIn_OrderRowComponent = await _uow.ProductsIn_OrderRowComponents.FindAsync(id);
                //.Include(p => p.OrderRowComponent)
                //.Include(p => p.Product)
                //.SingleOrDefaultAsync(m => m.ProductIn_OrderRowComponentId == id);
            if (productIn_OrderRowComponent == null)
            {
                return NotFound();
            }

            return View(productIn_OrderRowComponent);
        }

        // POST: ProductIn_OrderRowComponent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productIn_OrderRowComponent = await _uow.ProductsIn_OrderRowComponents.FindAsync(id);
            _uow.ProductsIn_OrderRowComponents.Remove(productIn_OrderRowComponent);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool ProductIn_OrderRowComponentExists(int id)
        //{
        //    return _uow.ProductsIn_OrderRowComponents.Any(e => e.ProductIn_OrderRowComponentId == id);
        //}
    }
}
