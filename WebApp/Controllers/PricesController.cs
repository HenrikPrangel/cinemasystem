﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class PricesController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public PricesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Prices
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Prices.Include(p => p.Campaign).Include(p => p.Currency).Include(p => p.Product).Include(p => p.Ticket);
            return View(await _uow.Prices.AllAsync());
        }

        // GET: Prices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var price = await _uow.Prices.FindAsync(id);
                //.Include(p => p.Campaign)
                //.Include(p => p.Currency)
                //.Include(p => p.Product)
                //.Include(p => p.Ticket)
                //.SingleOrDefaultAsync(m => m.PriceId == id);
            if (price == null)
            {
                return NotFound();
            }

            return View(price);
        }

        // GET: Prices/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignId");
            //ViewData["CurrencyId"] = new SelectList(_uow.Currencies, "CurrencyId", "CurrencyId");
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId");
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId");
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CurrencyId"] = new SelectList(await _uow.Currencies.AllAsync(), nameof(Currency.CurrencyId), nameof(Currency.CurrencyCode));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            return View();
        }

        // POST: Prices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PriceId,PriceValue,CurrencyId,TicketId,CampaignId,ProductId")] Price price)
        {
            if (ModelState.IsValid)
            {
                _uow.Prices.Add(price);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignId", price.CampaignId);
            //ViewData["CurrencyId"] = new SelectList(_uow.Currencies, "CurrencyId", "CurrencyId", price.CurrencyId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId", price.ProductId);
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId", price.TicketId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CurrencyId"] = new SelectList(await _uow.Currencies.AllAsync(), nameof(Currency.CurrencyId), nameof(Currency.CurrencyCode));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            return View(price);
        }

        // GET: Prices/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var price = await _uow.Prices.FindAsync(id);
            if (price == null)
            {
                return NotFound();
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignId", price.CampaignId);
            //ViewData["CurrencyId"] = new SelectList(_uow.Currencies, "CurrencyId", "CurrencyId", price.CurrencyId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId", price.ProductId);
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId", price.TicketId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CurrencyId"] = new SelectList(await _uow.Currencies.AllAsync(), nameof(Currency.CurrencyId), nameof(Currency.CurrencyCode));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            return View(price);
        }

        // POST: Prices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PriceId,PriceValue,CurrencyId,TicketId,CampaignId,ProductId")] Price price)
        {
            if (id != price.PriceId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Prices.Update(price);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Prices.Exists(price.PriceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignId", price.CampaignId);
            //ViewData["CurrencyId"] = new SelectList(_uow.Currencies, "CurrencyId", "CurrencyId", price.CurrencyId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductId", price.ProductId);
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId", price.TicketId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CurrencyId"] = new SelectList(await _uow.Currencies.AllAsync(), nameof(Currency.CurrencyId), nameof(Currency.CurrencyCode));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            return View(price);
        }

        // GET: Prices/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var price = await _uow.Prices.FindAsync(id);
                //.Include(p => p.Campaign)
                //.Include(p => p.Currency)
                //.Include(p => p.Product)
                //.Include(p => p.Ticket)
                //.SingleOrDefaultAsync(m => m.PriceId == id);
            if (price == null)
            {
                return NotFound();
            }

            return View(price);
        }

        // POST: Prices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var price = await _uow.Prices.FindAsync(id);
            _uow.Prices.Remove(price);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool PriceExists(int id)
        //{
        //    return _uow.Prices.Any(e => e.PriceId == id);
        //}
    }
}
