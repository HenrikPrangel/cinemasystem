﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class DiscountsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public DiscountsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Discounts
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _context.Discounts.Include(d => d.Campaign).Include(d => d.DiscountType).Include(d => d.Order).Include(d => d.Product).Include(d => d.ProductGroup).Include(d => d.Ticket).Include(d => d.UserGroup);
            return View(await _uow.Discounts.AllAsync());
        }

        // GET: Discounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discount = await _uow.Discounts.FindAsync(id);
                //.Include(d => d.Campaign)
                //.Include(d => d.DiscountType)
                //.Include(d => d.Order)
                //.Include(d => d.Product)
                //.Include(d => d.ProductGroup)
                //.Include(d => d.Ticket)
                //.Include(d => d.UserGroup)
                //.SingleOrDefaultAsync(m => m.DiscountId == id);
            if (discount == null)
            {
                return NotFound();
            }

            return View(discount);
        }

        // GET: Discounts/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName");
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName");
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId");
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName");
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupName");
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId");
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName");
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View();
        }

        // POST: Discounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DiscountId,DisccountName,DiscountValue,CampaignId,UserGroupId,OrderId,ProductId,ProductGroupId,DiscountTypeId,TicketId")] Discount discount)
        {
            if (ModelState.IsValid)
            {
                _uow.Discounts.Add(discount);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName", discount.CampaignId);
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName", discount.DiscountTypeId);
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", discount.OrderId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName", discount.ProductId);
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupName", discount.ProductGroupId);
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId", discount.TicketId);
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", discount.UserGroupId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(discount);
        }

        // GET: Discounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discount = await _uow.Discounts.FindAsync(id);
            if (discount == null)
            {
                return NotFound();
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName", discount.CampaignId);
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName", discount.DiscountTypeId);
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", discount.OrderId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName", discount.ProductId);
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupName", discount.ProductGroupId);
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId", discount.TicketId);
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", discount.UserGroupId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(discount);
        }

        // POST: Discounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DiscountId,DisccountName,DiscountValue,CampaignId,UserGroupId,OrderId,ProductId,ProductGroupId,DiscountTypeId,TicketId")] Discount discount)
        {
            if (id != discount.DiscountId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Discounts.Update(discount);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Discounts.Exists(discount.DiscountId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName", discount.CampaignId);
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName", discount.DiscountTypeId);
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", discount.OrderId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName", discount.ProductId);
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupName", discount.ProductGroupId);
            //ViewData["TicketId"] = new SelectList(_uow.Tickets, "TicketId", "TicketId", discount.TicketId);
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", discount.UserGroupId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            ViewData["TicketId"] = new SelectList(await _uow.Tickets.AllAsync(), nameof(Ticket.TicketId), nameof(Ticket.TicketId));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(discount);
        }

        // GET: Discounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discount = await _uow.Discounts.FindAsync(id);
                //.Include(d => d.Campaign)
                //.Include(d => d.DiscountType)
                //.Include(d => d.Order)
                //.Include(d => d.Product)
                //.Include(d => d.ProductGroup)
                //.Include(d => d.Ticket)
                //.Include(d => d.UserGroup)
                //.SingleOrDefaultAsync(m => m.DiscountId == id);
            if (discount == null)
            {
                return NotFound();
            }

            return View(discount);
        }

        // POST: Discounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var discount = await _uow.Discounts.FindAsync(id);
            _uow.Discounts.Remove(discount);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool DiscountExists(int id)
        //{
        //    return _uow.Discounts.Any(e => e.DiscountId == id);
        //}
    }
}
