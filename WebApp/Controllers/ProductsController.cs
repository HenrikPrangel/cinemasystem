﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public ProductsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Products.Include(p => p.ProductGroup);
            return View(await _uow.Products.AllAsync());
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _uow.Products.FindAsync(id);
                //.Include(p => p.ProductGroup)
                //.SingleOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupId");
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,ProductName,ProductGroupId")] Product product)
        {
            if (ModelState.IsValid)
            {
                _uow.Products.Add(product);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupId", product.ProductGroupId);
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _uow.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupId", product.ProductGroupId);
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,ProductName,ProductGroupId")] Product product)
        {
            if (id != product.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Products.Update(product);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Products.Exists(product.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["ProductGroupId"] = new SelectList(_uow.ProductGroups, "ProductGroupId", "ProductGroupId", product.ProductGroupId);
            ViewData["ProductGroupId"] = new SelectList(await _uow.ProductGroups.AllAsync(), nameof(ProductGroup.ProductGroupId), nameof(ProductGroup.ProductGroupName));
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _uow.Products.FindAsync(id);
                //.Include(p => p.ProductGroup)
                //.SingleOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _uow.Products.FindAsync(id);
            _uow.Products.Remove(product);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool ProductExists(int id)
        //{
        //    return _uow.Products.Any(e => e.ProductId == id);
        //}
    }
}
