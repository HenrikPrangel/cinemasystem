﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class InfoTypesController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public InfoTypesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: InfoTypes
        public async Task<IActionResult> Index()
        {
            return View(await _uow.InfoTypes.AllAsync());
        }

        // GET: InfoTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoType = await _uow.InfoTypes.FindAsync(id);
            if (infoType == null)
            {
                return NotFound();
            }

            return View(infoType);
        }

        // GET: InfoTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InfoTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InfoTypeId,InfoTypeName,InfoTypeDescription")] InfoType infoType)
        {
            if (ModelState.IsValid)
            {
                _uow.InfoTypes.Add(infoType);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(infoType);
        }

        // GET: InfoTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoType = await _uow.InfoTypes.FindAsync(id);
            if (infoType == null)
            {
                return NotFound();
            }
            return View(infoType);
        }

        // POST: InfoTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InfoTypeId,InfoTypeName,InfoTypeDescription")] InfoType infoType)
        {
            if (id != infoType.InfoTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.InfoTypes.Update(infoType);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.InfoTypes.Exists(infoType.InfoTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(infoType);
        }

        // GET: InfoTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoType = await _uow.InfoTypes.FindAsync(id);
            if (infoType == null)
            {
                return NotFound();
            }

            return View(infoType);
        }

        // POST: InfoTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var infoType = await _uow.InfoTypes.FindAsync(id);
            _uow.InfoTypes.Remove(infoType);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool InfoTypeExists(int id)
        //{
        //    return _uow.InfoTypes.Any(e => e.InfoTypeId == id);
        //}
    }
}
