﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class OrderRowComponentsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public OrderRowComponentsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: OrderRowComponents
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.OrderRowComponents.Include(o => o.OrderRow);
            return View(await _uow.OrderRowComponents.AllAsync());
        }

        // GET: OrderRowComponents/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRowComponent = await _uow.OrderRowComponents.FindAsync(id);
                //.Include(o => o.OrderRow)
                //.SingleOrDefaultAsync(m => m.OrderRowComponentId == id);
            if (orderRowComponent == null)
            {
                return NotFound();
            }

            return View(orderRowComponent);
        }

        // GET: OrderRowComponents/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId");
            ViewData["OrderRowId"] = new SelectList(await _uow.OrderRows.AllAsync(), nameof(OrderRow.OrderRowId), nameof(OrderRow.OrderId));
            return View();
        }

        // POST: OrderRowComponents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderRowComponentId,Description_OrderRowComponent,OrderRowComponentTotal,OrderRowId")] OrderRowComponent orderRowComponent)
        {
            if (ModelState.IsValid)
            {
                _uow.OrderRowComponents.Add(orderRowComponent);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId", orderRowComponent.OrderRowId);
            ViewData["OrderRowId"] = new SelectList(await _uow.OrderRows.AllAsync(), nameof(OrderRow.OrderRowId), nameof(OrderRow.OrderId));
            return View(orderRowComponent);
        }

        // GET: OrderRowComponents/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRowComponent = await _uow.OrderRowComponents.FindAsync(id);
            if (orderRowComponent == null)
            {
                return NotFound();
            }
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId", orderRowComponent.OrderRowId);

            return View(orderRowComponent);
        }

        // POST: OrderRowComponents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderRowComponentId,Description_OrderRowComponent,OrderRowComponentTotal,OrderRowId")] OrderRowComponent orderRowComponent)
        {
            if (id != orderRowComponent.OrderRowComponentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.OrderRowComponents.Update(orderRowComponent);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.OrderRowComponents.Exists(orderRowComponent.OrderRowComponentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId", orderRowComponent.OrderRowId);
            ViewData["OrderRowId"] = new SelectList(await _uow.OrderRows.AllAsync(), nameof(OrderRow.OrderRowId), nameof(OrderRow.OrderId));

            return View(orderRowComponent);
        }

        // GET: OrderRowComponents/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRowComponent = await _uow.OrderRowComponents.FindAsync(id);
                //.Include(o => o.OrderRow)
                //.SingleOrDefaultAsync(m => m.OrderRowComponentId == id);
            if (orderRowComponent == null)
            {
                return NotFound();
            }

            return View(orderRowComponent);
        }

        // POST: OrderRowComponents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var orderRowComponent = await _uow.OrderRowComponents.FindAsync(id);
            _uow.OrderRowComponents.Remove(orderRowComponent);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool OrderRowComponentExists(int id)
        //{
        //    return _uow.OrderRowComponents.Any(e => e.OrderRowComponentId == id);
        //}
    }
}
