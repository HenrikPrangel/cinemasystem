﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class UsersController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public UsersController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Users.Include(u => u.UserGroup);
            return View(await _uow.Users.AllAsync());
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _uow.Users.FindAsync(id);
                //.Include(u => u.UserGroup)
                //.SingleOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName");
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,FirstName,LastName,Password,JoinDate,LanguagePreferance,UserGroupId")] User user)
        {
            if (ModelState.IsValid)
            {
                _uow.Users.Add(user);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", user.UserGroupId);
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _uow.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", user.UserGroupId);
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserId,FirstName,LastName,Password,JoinDate,LanguagePreferance,UserGroupId")] User user)
        {
            if (id != user.UserId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Users.Update(user);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Users.Exists(user.UserId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", user.UserGroupId);
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));

            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _uow.Users.FindAsync(id);
                //.Include(u => u.UserGroup)
                //.SingleOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _uow.Users.FindAsync(id);
            _uow.Users.Remove(user);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool UserExists(int id)
        //{
        //    return _uow.Users.Any(e => e.UserId == id);
        //}
    }
}
