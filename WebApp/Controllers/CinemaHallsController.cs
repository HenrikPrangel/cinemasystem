﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class CinemaHallsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public CinemaHallsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: CinemaHalls
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.CinemaHalls.Include(c => c.Cinema);
            return View(await _uow.CinemaHalls.AllAsync());
        }

        // GET: CinemaHalls/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) 
            {
                return NotFound();
            }

            var cinemaHall = await _uow.CinemaHalls.FindAsync(id);
                //.Include(c => c.Cinema)
                //.SingleOrDefaultAsync(m => m.CinemaHallId == id);
            if (cinemaHall == null)
            {
                return NotFound();
            }

            return View(cinemaHall);
        }

        // GET: CinemaHalls/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address");
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));

            return View();
        }

        // POST: CinemaHalls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CinemaHallId,CinemaHallName,SeatCount,CinemaId")] CinemaHall cinemaHall)
        {
            if (ModelState.IsValid)
            {
                await _uow.CinemaHalls.AddAsync(cinemaHall);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address", cinemaHall.CinemaId);
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName), cinemaHall.CinemaId);

            return View(cinemaHall);
        }

        // GET: CinemaHalls/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cinemaHall = await _uow.CinemaHalls.FindAsync(id);
            if (cinemaHall == null)
            {
                return NotFound();
            }
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address", cinemaHall.CinemaId);
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            return View(cinemaHall);
        }

        // POST: CinemaHalls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CinemaHallId,CinemaHallName,SeatCount,CinemaId")] CinemaHall cinemaHall)
        {
            if (id != cinemaHall.CinemaHallId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.CinemaHalls.Update(cinemaHall);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.CinemaHalls.Exists(cinemaHall.CinemaHallId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address", cinemaHall.CinemaId);
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            return View(cinemaHall);
        }

        // GET: CinemaHalls/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cinemaHall = await _uow.CinemaHalls.FindAsync(id);
                //.Include(c => c.Cinema)
                //.SingleOrDefaultAsync(m => m.CinemaHallId == id);
            if (cinemaHall == null)
            {
                return NotFound();
            }

            return View(cinemaHall);
        }

        // POST: CinemaHalls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cinemaHall = await _uow.CinemaHalls.FindAsync(id);
            _uow.CinemaHalls.Remove(cinemaHall);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool CinemaHallExists(int id)
        //{
        //    return _uow.CinemaHalls.Any(e => e.CinemaHallId == id);
        //}
    }
}
