﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class TicketsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public TicketsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Tickets
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Tickets.Include(t => t.OrderRow).Include(t => t.Seat).Include(t => t.Session);
            return View(await _uow.Tickets.AllAsync());
        }

        // GET: Tickets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _uow.Tickets.FindAsync(id);
                //.Include(t => t.OrderRow)
                //.Include(t => t.Seat)
                //.Include(t => t.Session)
                //.SingleOrDefaultAsync(m => m.TicketId == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // GET: Tickets/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId");
            //ViewData["SeatId"] = new SelectList(_uow.Seats, "SeatId", "SeatId");
            //ViewData["SessionId"] = new SelectList(_uow.Sessions, "SessionId", "SessionId");
            ViewData["OrderRowId"] = new SelectList(await _uow.OrderRows.AllAsync(), nameof(OrderRow.OrderRowId), nameof(OrderRow.OrderRowId));
            ViewData["SeatId"] = new SelectList(await _uow.Seats.AllAsync(), nameof(Seat.SeatId), nameof(Seat.SeatNumber));
            ViewData["SessionId"] = new SelectList(await _uow.Sessions.AllAsync(), nameof(Session.SessionId), nameof(Session.SessionId));
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TicketId,TicketCinemaName,TicketMovieName,TicketMovieDate,TicketRow,TicketSeat,TicketMovieAgeRestriction,TicketPrice,TicketHolder,SessionId,SeatId,OrderRowId")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                _uow.Tickets.Add(ticket);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId", ticket.OrderRowId);
            //ViewData["SeatId"] = new SelectList(_uow.Seats, "SeatId", "SeatId", ticket.SeatId);
            //ViewData["SessionId"] = new SelectList(_uow.Sessions, "SessionId", "SessionId", ticket.SessionId);
            ViewData["OrderRowId"] = new SelectList(await _uow.OrderRows.AllAsync(), nameof(OrderRow.OrderRowId), nameof(OrderRow.OrderRowId));
            ViewData["SeatId"] = new SelectList(await _uow.Seats.AllAsync(), nameof(Seat.SeatId), nameof(Seat.SeatNumber));
            ViewData["SessionId"] = new SelectList(await _uow.Sessions.AllAsync(), nameof(Session.SessionId), nameof(Session.SessionId));
            return View(ticket);
        }

        // GET: Tickets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _uow.Tickets.FindAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId", ticket.OrderRowId);
            //ViewData["SeatId"] = new SelectList(_uow.Seats, "SeatId", "SeatId", ticket.SeatId);
            //ViewData["SessionId"] = new SelectList(_uow.Sessions, "SessionId", "SessionId", ticket.SessionId);
            ViewData["OrderRowId"] = new SelectList(await _uow.OrderRows.AllAsync(), nameof(OrderRow.OrderRowId), nameof(OrderRow.OrderRowId));
            ViewData["SeatId"] = new SelectList(await _uow.Seats.AllAsync(), nameof(Seat.SeatId), nameof(Seat.SeatNumber));
            ViewData["SessionId"] = new SelectList(await _uow.Sessions.AllAsync(), nameof(Session.SessionId), nameof(Session.SessionId));
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TicketId,TicketCinemaName,TicketMovieName,TicketMovieDate,TicketRow,TicketSeat,TicketMovieAgeRestriction,TicketPrice,TicketHolder,SessionId,SeatId,OrderRowId")] Ticket ticket)
        {
            if (id != ticket.TicketId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Tickets.Update(ticket);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Tickets.Exists(ticket.TicketId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderRowId"] = new SelectList(_uow.OrderRows, "OrderRowId", "OrderRowId", ticket.OrderRowId);
            //ViewData["SeatId"] = new SelectList(_uow.Seats, "SeatId", "SeatId", ticket.SeatId);
            //ViewData["SessionId"] = new SelectList(_uow.Sessions, "SessionId", "SessionId", ticket.SessionId);
            ViewData["OrderRowId"] = new SelectList(await _uow.OrderRows.AllAsync(), nameof(OrderRow.OrderRowId), nameof(OrderRow.OrderRowId));
            ViewData["SeatId"] = new SelectList(await _uow.Seats.AllAsync(), nameof(Seat.SeatId), nameof(Seat.SeatNumber));
            ViewData["SessionId"] = new SelectList(await _uow.Sessions.AllAsync(), nameof(Session.SessionId), nameof(Session.SessionId));
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _uow.Tickets.FindAsync(id);
                //.Include(t => t.OrderRow)
                //.Include(t => t.Seat)
                //.Include(t => t.Session)
                //.SingleOrDefaultAsync(m => m.TicketId == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ticket = await _uow.Tickets.FindAsync(id);
            _uow.Tickets.Remove(ticket);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool TicketExists(int id)
        //{
        //    return _uow.Tickets.Any(e => e.TicketId == id);
        //}
    }
}
