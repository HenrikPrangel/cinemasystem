﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class PaymentsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public PaymentsController(IAppUnitOfWork context)
        {
            _uow = context;
        }

        // GET: Payments
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Payments.Include(p => p.Order);
            return View(await _uow.Payments.AllAsync());
        }

        // GET: Payments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var payment = await _uow.Payments.FindAsync(id);
                //.Include(p => p.Order)
                //.SingleOrDefaultAsync(m => m.PaymentId == id);
            if (payment == null)
            {
                return NotFound();
            }

            return View(payment);
        }

        // GET: Payments/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId");
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View();
        }

        // POST: Payments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PaymentId,PaymentDate,OrderId")] Payment payment)
        {
            if (ModelState.IsValid)
            {
                _uow.Payments.Add(payment);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", payment.OrderId);
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View(payment);
        }

        // GET: Payments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var payment = await _uow.Payments.FindAsync(id);
            if (payment == null)
            {
                return NotFound();
            }
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", payment.OrderId);
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View(payment);
        }

        // POST: Payments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PaymentId,PaymentDate,OrderId")] Payment payment)
        {
            if (id != payment.PaymentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Payments.Update(payment);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Payments.Exists(payment.PaymentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", payment.OrderId);
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View(payment);
        }

        // GET: Payments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var payment = await _uow.Payments.FindAsync(id);
                //.Include(p => p.Order)
                //.SingleOrDefaultAsync(m => m.PaymentId == id);
            if (payment == null)
            {
                return NotFound();
            }

            return View(payment);
        }

        // POST: Payments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var payment = await _uow.Payments.FindAsync(id);
            _uow.Payments.Remove(payment);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool PaymentExists(int id)
        //{
        //    return _uow.Payments.Any(e => e.PaymentId == id);
        //}
    }
}
