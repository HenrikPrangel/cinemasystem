﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class SeatsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public SeatsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Seats
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Seats.Include(s => s.Row);
            return View(await _uow.Seats.AllAsync());
        }

        // GET: Seats/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seat = await _uow.Seats.FindAsync(id);
                //.Include(s => s.Row)
                //.SingleOrDefaultAsync(m => m.SeatId == id);
            if (seat == null)
            {
                return NotFound();
            }

            return View(seat);
        }

        // GET: Seats/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["RowId"] = new SelectList(_uow.Rows, "RowId", "RowId");
            ViewData["RowId"] = new SelectList(await _uow.Rows.AllAsync(), nameof(Row.RowId), nameof(Row.RowNumber));
            return View();
        }

        // POST: Seats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SeatId,SeatNumber,RowId")] Seat seat)
        {
            if (ModelState.IsValid)
            {
                _uow.Seats.Add(seat);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["RowId"] = new SelectList(_uow.Rows, "RowId", "RowId", seat.RowId);
            ViewData["RowId"] = new SelectList(await _uow.Rows.AllAsync(), nameof(Row.RowId), nameof(Row.RowNumber));
            return View(seat);
        }

        // GET: Seats/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seat = await _uow.Seats.FindAsync(id);
            if (seat == null)
            {
                return NotFound();
            }
            //ViewData["RowId"] = new SelectList(_uow.Rows, "RowId", "RowId", seat.RowId);
            ViewData["RowId"] = new SelectList(await _uow.Rows.AllAsync(), nameof(Row.RowId), nameof(Row.RowNumber));
            return View(seat);
        }

        // POST: Seats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SeatId,SeatNumber,RowId")] Seat seat)
        {
            if (id != seat.SeatId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Seats.Update(seat);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Seats.Exists(seat.SeatId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["RowId"] = new SelectList(_uow.Rows, "RowId", "RowId", seat.RowId);
            ViewData["RowId"] = new SelectList(await _uow.Rows.AllAsync(), nameof(Row.RowId), nameof(Row.RowNumber));
            return View(seat);
        }

        // GET: Seats/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seat = await _uow.Seats.FindAsync(id);
            //    .Include(s => s.Row)
            //    .SingleOrDefaultAsync(m => m.SeatId == id);
            if (seat == null)
            {
                return NotFound();
            }

            return View(seat);
        }

        // POST: Seats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var seat = await _uow.Seats.FindAsync(id);
            _uow.Seats.Remove(seat);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool SeatExists(int id)
        //{
        //    return _uow.Seats.Any(e => e.SeatId == id);
        //}
    }
}
