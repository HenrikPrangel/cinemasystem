﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class OrderRowsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public OrderRowsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: OrderRows
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.OrderRows.Include(o => o.Order);
            return View(await _uow.OrderRows.AllAsync());
        }

        // GET: OrderRows/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRow = await _uow.OrderRows.FindAsync(id);
                //.Include(o => o.Order)
                //.SingleOrDefaultAsync(m => m.OrderRowId == id);
            if (orderRow == null)
            {
                return NotFound();
            }

            return View(orderRow);
        }

        // GET: OrderRows/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId");
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View();
        }

        // POST: OrderRows/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderRowId,Quantity,OtherPriceTotal,TicketPriceTotal,OrderRowTotalPrice,OrderId")] OrderRow orderRow)
        {
            if (ModelState.IsValid)
            {
                _uow.OrderRows.Add(orderRow);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", orderRow.OrderId);
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View(orderRow);
        }

        // GET: OrderRows/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRow = await _uow.OrderRows.FindAsync(id);
            if (orderRow == null)
            {
                return NotFound();
            }
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", orderRow.OrderId);
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View(orderRow);
        }

        // POST: OrderRows/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderRowId,Quantity,OtherPriceTotal,TicketPriceTotal,OrderRowTotalPrice,OrderId")] OrderRow orderRow)
        {
            if (id != orderRow.OrderRowId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.OrderRows.Update(orderRow);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.OrderRows.Exists(orderRow.OrderRowId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["OrderId"] = new SelectList(_uow.Orders, "OrderId", "OrderId", orderRow.OrderId);
            ViewData["OrderId"] = new SelectList(await _uow.Orders.AllAsync(), nameof(Order.OrderId), nameof(Order.OrderId));
            return View(orderRow);
        }

        // GET: OrderRows/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderRow = await _uow.OrderRows.FindAsync(id);
                //.Include(o => o.Order)
                //.SingleOrDefaultAsync(m => m.OrderRowId == id);
            if (orderRow == null)
            {
                return NotFound();
            }

            return View(orderRow);
        }

        // POST: OrderRows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var orderRow = await _uow.OrderRows.FindAsync(id);
            _uow.OrderRows.Remove(orderRow);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool OrderRowExists(int id)
        //{
        //    return _uow.OrderRows.Any(e => e.OrderRowId == id);
        //}
    }
}
