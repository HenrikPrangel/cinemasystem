﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class MovieInCinemasController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public MovieInCinemasController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: MovieInCinemas
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.MoviesInCinema.Include(m => m.Cinema).Include(m => m.Movie);
            return View(await _uow.MoviesInCinemas.AllAsync());
        }

        // GET: MovieInCinemas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieInCinema = await _uow.MoviesInCinemas.FindAsync(id);
            //.Include(m => m.Cinema)
            //.Include(m => m.Movie)
            //.SingleOrDefaultAsync(m => m.MovieInCinemaId == id);
            if (movieInCinema == null)
            {
                return NotFound();
            }

            return View(movieInCinema);
        }

        // GET: MovieInCinemas/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "CinemaId");
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieId");
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            return View();
        }

        // POST: MovieInCinemas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MovieInCinemaId,InCinemaFrom,InCinemaUntil,CinemaId,MovieId")] MovieInCinema movieInCinema)
        {
            if (ModelState.IsValid)
            {
                _uow.MoviesInCinemas.Add(movieInCinema);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "CinemaId", movieInCinema.CinemaId);
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieId", movieInCinema.MovieId);
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            return View(movieInCinema);
        }

        // GET: MovieInCinemas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieInCinema = await _uow.MoviesInCinemas.FindAsync(id);
            if (movieInCinema == null)
            {
                return NotFound();
            }
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "CinemaId", movieInCinema.CinemaId);
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieId", movieInCinema.MovieId);
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            return View(movieInCinema);
        }

        // POST: MovieInCinemas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MovieInCinemaId,InCinemaFrom,InCinemaUntil,CinemaId,MovieId")] MovieInCinema movieInCinema)
        {
            if (id != movieInCinema.MovieInCinemaId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.MoviesInCinemas.Update(movieInCinema);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.MoviesInCinemas.Exists(movieInCinema.MovieInCinemaId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "CinemaId", movieInCinema.CinemaId);
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieId", movieInCinema.MovieId);
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            return View(movieInCinema);
        }

        // GET: MovieInCinemas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieInCinema = await _uow.MoviesInCinemas.FindAsync(id);
                //.Include(m => m.Cinema)
                //.Include(m => m.Movie)
                //.SingleOrDefaultAsync(m => m.MovieInCinemaId == id);
            if (movieInCinema == null)
            {
                return NotFound();
            }

            return View(movieInCinema);
        }

        // POST: MovieInCinemas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var movieInCinema = await _uow.MoviesInCinemas.FindAsync(id);
            _uow.MoviesInCinemas.Remove(movieInCinema);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool MovieInCinemaExists(int id)
        //{
        //    return _uow.MoviesInCinema.Any(e => e.MovieInCinemaId == id);
        //}
    }
}
