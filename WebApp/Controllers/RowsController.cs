﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class RowsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public RowsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Rows
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Rows.Include(r => r.CinemaHall);
            return View(await _uow.Rows.AllAsync());
        }

        // GET: Rows/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var row = await _uow.Rows.FindAsync(id);
                //.Include(r => r.CinemaHall)
                //.SingleOrDefaultAsync(m => m.RowId == id);
            if (row == null)
            {
                return NotFound();
            }

            return View(row);
        }

        // GET: Rows/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallName");
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            return View();
        }

        // POST: Rows/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RowId,RowNumber,SeatAmount,CinemaHallId")] Row row)
        {
            if (ModelState.IsValid)
            {
                _uow.Rows.Add(row);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallName", row.CinemaHallId);
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            return View(row);
        }

        // GET: Rows/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var row = await _uow.Rows.FindAsync(id);
            if (row == null)
            {
                return NotFound();
            }
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallName", row.CinemaHallId);
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            return View(row);
        }

        // POST: Rows/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RowId,RowNumber,SeatAmount,CinemaHallId")] Row row)
        {
            if (id != row.RowId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Rows.Update(row);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Rows.Exists(row.RowId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallName", row.CinemaHallId);
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            return View(row);
        }

        // GET: Rows/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var row = await _uow.Rows.FindAsync(id);
                //.Include(r => r.CinemaHall)
                //.SingleOrDefaultAsync(m => m.RowId == id);
            if (row == null)
            {
                return NotFound();
            }

            return View(row);
        }

        // POST: Rows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var row = await _uow.Rows.FindAsync(id);
            _uow.Rows.Remove(row);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool RowExists(int id)
        //{
        //    return _uow.Rows.Any(e => e.RowId == id);
        //}
    }
}
