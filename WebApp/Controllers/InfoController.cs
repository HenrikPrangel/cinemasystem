﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class InfoController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public InfoController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Info
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Info.Include(i => i.Campaign).Include(i => i.Cinema).Include(i => i.Discount).Include(i => i.DiscountType).Include(i => i.InfoType).Include(i => i.Movie).Include(i => i.Product).Include(i => i.UserGroup);
            return View(await _uow.Info.AllAsync());
        }

        // GET: Info/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var info = await _uow.Info.FindAsync(id);
                //.Include(i => i.Campaign)
                //.Include(i => i.Cinema)
                //.Include(i => i.Discount)
                //.Include(i => i.DiscountType)
                //.Include(i => i.InfoType)
                //.Include(i => i.Movie)
                //.Include(i => i.Product)
                //.Include(i => i.UserGroup)
                //.SingleOrDefaultAsync(m => m.InfoId == id);
            if (info == null)
            {
                return NotFound();
            }

            return View(info);
        }

        // GET: Info/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName");
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address");
            //ViewData["DiscountId"] = new SelectList(_uow.Discounts, "DiscountId", "DisccountName");
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName");
            //ViewData["InfoTypeId"] = new SelectList(_uow.InfoTypes, "InfoTypeId", "InfoTypeName");
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieName");
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName");
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName");
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["DiscountId"] = new SelectList(await _uow.Discounts.AllAsync(), nameof(Discount.DiscountId), nameof(Discount.DisccountName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["InfoTypeId"] = new SelectList(await _uow.InfoTypes.AllAsync(), nameof(InfoType.InfoTypeId), nameof(InfoType.InfoTypeName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));

            return View();
        }

        // POST: Info/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InfoId,Description,InfoTypeId,CinemaId,MovieId,ProductId,CampaignId,UserGroupId,DiscountId,DiscountTypeId")] Info info)
        {
            if (ModelState.IsValid)
            {
                _uow.Info.Add(info);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName", info.CampaignId);
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address", info.CinemaId);
            //ViewData["DiscountId"] = new SelectList(_uow.Discounts, "DiscountId", "DisccountName", info.DiscountId);
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName", info.DiscountTypeId);
            //ViewData["InfoTypeId"] = new SelectList(_uow.InfoTypes, "InfoTypeId", "InfoTypeName", info.InfoTypeId);
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieName", info.MovieId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName", info.ProductId);
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", info.UserGroupId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["DiscountId"] = new SelectList(await _uow.Discounts.AllAsync(), nameof(Discount.DiscountId), nameof(Discount.DisccountName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["InfoTypeId"] = new SelectList(await _uow.InfoTypes.AllAsync(), nameof(InfoType.InfoTypeId), nameof(InfoType.InfoTypeName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(info);
        }

        // GET: Info/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var info = await _uow.Info.FindAsync(id);
            if (info == null)
            {
                return NotFound();
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName", info.CampaignId);
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address", info.CinemaId);
            //ViewData["DiscountId"] = new SelectList(_uow.Discounts, "DiscountId", "DisccountName", info.DiscountId);
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName", info.DiscountTypeId);
            //ViewData["InfoTypeId"] = new SelectList(_uow.InfoTypes, "InfoTypeId", "InfoTypeName", info.InfoTypeId);
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieName", info.MovieId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName", info.ProductId);
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", info.UserGroupId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["DiscountId"] = new SelectList(await _uow.Discounts.AllAsync(), nameof(Discount.DiscountId), nameof(Discount.DisccountName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["InfoTypeId"] = new SelectList(await _uow.InfoTypes.AllAsync(), nameof(InfoType.InfoTypeId), nameof(InfoType.InfoTypeName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(info);
        }

        // POST: Info/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InfoId,Description,InfoTypeId,CinemaId,MovieId,ProductId,CampaignId,UserGroupId,DiscountId,DiscountTypeId")] Info info)
        {
            if (id != info.InfoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Info.Update(info);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Info.Exists(info.InfoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CampaignId"] = new SelectList(_uow.Campaigns, "CampaignId", "CampaignName", info.CampaignId);
            //ViewData["CinemaId"] = new SelectList(_uow.Cinemas, "CinemaId", "Address", info.CinemaId);
            //ViewData["DiscountId"] = new SelectList(_uow.Discounts, "DiscountId", "DisccountName", info.DiscountId);
            //ViewData["DiscountTypeId"] = new SelectList(_uow.DiscountTypes, "DiscountTypeId", "DiscountTypeName", info.DiscountTypeId);
            //ViewData["InfoTypeId"] = new SelectList(_uow.InfoTypes, "InfoTypeId", "InfoTypeName", info.InfoTypeId);
            //ViewData["MovieId"] = new SelectList(_uow.Movies, "MovieId", "MovieName", info.MovieId);
            //ViewData["ProductId"] = new SelectList(_uow.Products, "ProductId", "ProductName", info.ProductId);
            //ViewData["UserGroupId"] = new SelectList(_uow.UserGroups, "UserGroupId", "UserGroupName", info.UserGroupId);
            ViewData["CampaignId"] = new SelectList(await _uow.Campaigns.AllAsync(), nameof(Campaign.CampaignId), nameof(Campaign.CampaignName));
            ViewData["CinemaId"] = new SelectList(await _uow.Cinemas.AllAsync(), nameof(Cinema.CinemaId), nameof(Cinema.CinemaName));
            ViewData["DiscountId"] = new SelectList(await _uow.Discounts.AllAsync(), nameof(Discount.DiscountId), nameof(Discount.DisccountName));
            ViewData["DiscountTypeId"] = new SelectList(await _uow.DiscountTypes.AllAsync(), nameof(DiscountType.DiscountTypeId), nameof(DiscountType.DiscountTypeName));
            ViewData["InfoTypeId"] = new SelectList(await _uow.InfoTypes.AllAsync(), nameof(InfoType.InfoTypeId), nameof(InfoType.InfoTypeName));
            ViewData["MovieId"] = new SelectList(await _uow.Movies.AllAsync(), nameof(Movie.MovieId), nameof(Movie.MovieName));
            ViewData["ProductId"] = new SelectList(await _uow.Products.AllAsync(), nameof(Product.ProductId), nameof(Product.ProductName));
            ViewData["UserGroupId"] = new SelectList(await _uow.UserGroups.AllAsync(), nameof(UserGroup.UserGroupId), nameof(UserGroup.UserGroupName));
            return View(info);
        }

        // GET: Info/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var info = await _uow.Info.FindAsync(id);
                //.Include(i => i.Campaign)
                //.Include(i => i.Cinema)
                //.Include(i => i.Discount)
                //.Include(i => i.DiscountType)
                //.Include(i => i.InfoType)
                //.Include(i => i.Movie)
                //.Include(i => i.Product)
                //.Include(i => i.UserGroup)
                //.SingleOrDefaultAsync(m => m.InfoId == id);
            if (info == null)
            {
                return NotFound();
            }

            return View(info);
        }

        // POST: Info/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var info = await _uow.Info.FindAsync(id);
            _uow.Info.Remove(info);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool InfoExists(int id)
        //{
        //    return _uow.Info.Any(e => e.InfoId == id);
        //}
    }
}
