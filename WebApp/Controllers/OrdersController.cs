﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public OrdersController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Orders
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Orders.Include(o => o.User);
            return View(await _uow.Orders.AllAsync());
        }

        // GET: Orders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _uow.Orders.FindAsync(id);
                //.Include(o => o.User)
                //.SingleOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["UserId"] = new SelectList(_uow.Users, "UserId", "UserId");
            ViewData["UserId"] = new SelectList(await _uow.Users.AllAsync(), nameof(Domain.User.UserId), nameof(Domain.User.FirstLastName));
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderId,OrderTotal,UserId")] Order order)
        {
            if (ModelState.IsValid)
            {
                _uow.Orders.Add(order);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["UserId"] = new SelectList(_uow.Users, "UserId", "UserId", order.UserId);
            ViewData["UserId"] = new SelectList(await _uow.Users.AllAsync(), nameof(Domain.User.UserId), nameof(Domain.User.FirstLastName));
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _uow.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            //ViewData["UserId"] = new SelectList(_uow.Users, "UserId", "UserId", order.UserId);
            ViewData["UserId"] = new SelectList(await _uow.Users.AllAsync(), nameof(Domain.User.UserId), nameof(Domain.User.FirstLastName));
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderId,OrderTotal,UserId")] Order order)
        {
            if (id != order.OrderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Orders.Update(order);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Orders.Exists(order.OrderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["UserId"] = new SelectList(_uow.Users, "UserId", "UserId", order.UserId);
            ViewData["UserId"] = new SelectList(await _uow.Users.AllAsync(), nameof(Domain.User.UserId), nameof(Domain.User.FirstLastName));
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _uow.Orders.FindAsync(id);
                //.Include(o => o.User)
                //.SingleOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _uow.Orders.FindAsync(id);
            _uow.Orders.Remove(order);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool OrderExists(int id)
        //{
        //    return _uow.Orders.Any(e => e.OrderId == id);
        //}
    }
}
