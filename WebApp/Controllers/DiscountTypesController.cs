﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class DiscountTypesController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public DiscountTypesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: DiscountTypes
        public async Task<IActionResult> Index()
        {
            return View(await _uow.DiscountTypes.AllAsync());
        }

        // GET: DiscountTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discountType = await _uow.DiscountTypes.FindAsync(id);
            if (discountType == null)
            {
                return NotFound();
            }

            return View(discountType);
        }

        // GET: DiscountTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DiscountTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DiscountTypeId,DiscountTypeName")] DiscountType discountType)
        {
            if (ModelState.IsValid)
            {
                _uow.DiscountTypes.Add(discountType);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(discountType);
        }

        // GET: DiscountTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discountType = await _uow.DiscountTypes.FindAsync(id);
            if (discountType == null)
            {
                return NotFound();
            }
            return View(discountType);
        }

        // POST: DiscountTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DiscountTypeId,DiscountTypeName")] DiscountType discountType)
        {
            if (id != discountType.DiscountTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.DiscountTypes.Update(discountType);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.DiscountTypes.Exists(discountType.DiscountTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(discountType);
        }

        // GET: DiscountTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var discountType = await _uow.DiscountTypes.FindAsync(id);
            if (discountType == null)
            {
                return NotFound();
            }

            return View(discountType);
        }

        // POST: DiscountTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var discountType = await _uow.DiscountTypes.FindAsync(id);
            _uow.DiscountTypes.Remove(discountType);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool DiscountTypeExists(int id)
        //{
        //    return _uow.DiscountTypes.Any(e => e.DiscountTypeId == id);
        //}
    }
}
