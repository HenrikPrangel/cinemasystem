﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;

namespace WebApp.Controllers
{
    public class SessionsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public SessionsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Sessions
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _uow.Sessions.Include(s => s.CinemaHall).Include(s => s.MovieInCinema);
            return View(await _uow.Sessions.AllAsync());
        }

        // GET: Sessions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var session = await _uow.Sessions.FindAsync(id);
                //.Include(s => s.CinemaHall)
                //.Include(s => s.MovieInCinema)
                //.SingleOrDefaultAsync(m => m.SessionId == id);
            if (session == null)
            {
                return NotFound();
            }

            return View(session);
        }

        // GET: Sessions/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallId");
            //ViewData["MovieInCinemaId"] = new SelectList(_uow.MoviesInCinema, "MovieInCinemaId", "MovieInCinemaId");
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            ViewData["MovieInCinemaId"] = new SelectList(await _uow.MoviesInCinemas.AllAsync(), nameof(MovieInCinema.MovieInCinemaId), nameof(MovieInCinema.MovieInCinemaId));
            return View();
        }

        // POST: Sessions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SessionId,SessionBegins,SessionEnds,MovieInCinemaId,CinemaHallId")] Session session)
        {
            if (ModelState.IsValid)
            {
                _uow.Sessions.Add(session);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallId", session.CinemaHallId);
            //ViewData["MovieInCinemaId"] = new SelectList(_uow.MoviesInCinema, "MovieInCinemaId", "MovieInCinemaId", session.MovieInCinemaId);
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            ViewData["MovieInCinemaId"] = new SelectList(await _uow.MoviesInCinemas.AllAsync(), nameof(MovieInCinema.MovieInCinemaId), nameof(MovieInCinema.MovieInCinemaId));
            return View(session);
        }

        // GET: Sessions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var session = await _uow.Sessions.FindAsync(id);
            if (session == null)
            {
                return NotFound();
            }
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallId", session.CinemaHallId);
            //ViewData["MovieInCinemaId"] = new SelectList(_uow.MoviesInCinema, "MovieInCinemaId", "MovieInCinemaId", session.MovieInCinemaId);
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            ViewData["MovieInCinemaId"] = new SelectList(await _uow.MoviesInCinemas.AllAsync(), nameof(MovieInCinema.MovieInCinemaId), nameof(MovieInCinema.MovieInCinemaId));
            return View(session);
        }

        // POST: Sessions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SessionId,SessionBegins,SessionEnds,MovieInCinemaId,CinemaHallId")] Session session)
        {
            if (id != session.SessionId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _uow.Sessions.Update(session);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_uow.Sessions.Exists(session.SessionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["CinemaHallId"] = new SelectList(_uow.CinemaHalls, "CinemaHallId", "CinemaHallId", session.CinemaHallId);
            //ViewData["MovieInCinemaId"] = new SelectList(_uow.MoviesInCinema, "MovieInCinemaId", "MovieInCinemaId", session.MovieInCinemaId);
            ViewData["CinemaHallId"] = new SelectList(await _uow.CinemaHalls.AllAsync(), nameof(CinemaHall.CinemaHallId), nameof(CinemaHall.CinemaHallName));
            ViewData["MovieInCinemaId"] = new SelectList(await _uow.MoviesInCinemas.AllAsync(), nameof(MovieInCinema.MovieInCinemaId), nameof(MovieInCinema.MovieInCinemaId));
            return View(session);
        }

        // GET: Sessions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var session = await _uow.Sessions.FindAsync(id);
                //.Include(s => s.CinemaHall)
                //.Include(s => s.MovieInCinema)
                //.SingleOrDefaultAsync(m => m.SessionId == id);
            if (session == null)
            {
                return NotFound();
            }

            return View(session);
        }

        // POST: Sessions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var session = await _uow.Sessions.FindAsync(id);
            _uow.Sessions.Remove(session);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool SessionExists(int id)
        //{
        //    return _uow.Sessions.Any(e => e.SessionId == id);
        //}
    }
}
