﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IUserRepository Users { get; }
        ICinemaHallRepository CinemaHalls { get; }
        IDiscountRepository Discounts { get; }
        IInfoRepository Info { get; }
        IMovieInCinemaRepository MoviesInCinemas { get; }
        IOrderRepository Orders { get; }
        IOrderRowRepository OrderRows { get; }
        IOrderRowComponentRepository OrderRowComponents { get; }
        IPaymentRepository Payments { get; }
        IPriceRepository Prices { get; }
        IProductRepository Products { get; }
        IProductIn_OrderRowComponentRepository ProductsIn_OrderRowComponents { get; }
        IRowRepository Rows { get; }
        ISeatRepository Seats { get; }
        ISessionRepository Sessions { get; }
        ITicketRepository Tickets { get; }

        IRepository<DiscountType> DiscountTypes { get; }
        IRepository<Campaign> Campaigns { get; }
        IRepository<Cinema> Cinemas { get; }
        IRepository<Currency> Currencies { get; }
        IRepository<InfoType> InfoTypes { get; }
        IRepository<Movie> Movies { get; }
        IRepository<ProductGroup> ProductGroups { get; }
        IRepository<UserGroup> UserGroups { get; }
    }
}
