﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Domain.User> Users { get; set; }
        public DbSet<Domain.Campaign> Campaigns { get; set; }
        public DbSet<Domain.Cinema> Cinemas { get; set; }
        public DbSet<Domain.CinemaHall> CinemaHalls { get; set; }
        public DbSet<Domain.Currency> Currencies { get; set; }
        public DbSet<Domain.Discount> Discounts { get; set; }
        public DbSet<Domain.DiscountType> DiscountTypes { get; set; }
        public DbSet<Domain.Info> Info { get; set; }
        public DbSet<Domain.InfoType> InfoTypes { get; set; }
        public DbSet<Domain.Movie> Movies { get; set; }
        public DbSet<Domain.MovieInCinema> MoviesInCinema { get; set; }
        public DbSet<Domain.Order> Orders { get; set; }
        public DbSet<Domain.OrderRow> OrderRows { get; set; }
        public DbSet<Domain.OrderRowComponent> OrderRowComponents { get; set; }
        public DbSet<Domain.Payment> Payments { get; set; }
        public DbSet<Domain.Price> Prices { get; set; }
        public DbSet<Domain.Product> Products { get; set; }
        public DbSet<Domain.ProductGroup> ProductGroups { get; set; }
        public DbSet<Domain.ProductIn_OrderRowComponent> ProductsIn_OrderRowComponents { get; set; }
        public DbSet<Domain.Row> Rows { get; set; }
        public DbSet<Domain.Seat> Seats { get; set; }
        public DbSet<Domain.Session> Sessions { get; set; }
        public DbSet<Domain.Ticket> Tickets { get; set; }
        public DbSet<Domain.UserGroup> UserGroups { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in 
                builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
