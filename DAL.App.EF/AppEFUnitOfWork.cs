﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces;
using DAL.App.Interfaces.Helpers;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.EF
{
    public class AppEFUnitOfWork : IAppUnitOfWork
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public AppEFUnitOfWork(IDataContext dataContext, IRepositoryProvider repositoryProvider)
        {
            _repositoryProvider = repositoryProvider;
            _applicationDbContext = dataContext as ApplicationDbContext;
            if (_applicationDbContext == null)
            {
                throw new NullReferenceException("No EF dbcontext found in UOW");
            }
        }

        public IUserRepository Users => GetCustomRepository<IUserRepository>();

        public ICinemaHallRepository CinemaHalls => GetCustomRepository<ICinemaHallRepository>();

        public IDiscountRepository Discounts => GetCustomRepository<IDiscountRepository>();

        public IInfoRepository Info => GetCustomRepository<IInfoRepository>();

        public IMovieInCinemaRepository MoviesInCinemas => GetCustomRepository<IMovieInCinemaRepository>();

        public IOrderRepository Orders => GetCustomRepository<IOrderRepository>();

        public IOrderRowRepository OrderRows => GetCustomRepository<IOrderRowRepository>();

        public IOrderRowComponentRepository OrderRowComponents => GetCustomRepository<IOrderRowComponentRepository>();

        public IPaymentRepository Payments => GetCustomRepository<IPaymentRepository>();

        public IPriceRepository Prices => GetCustomRepository<IPriceRepository>();

        public IProductRepository Products => GetCustomRepository<IProductRepository>();

        public IProductIn_OrderRowComponentRepository ProductsIn_OrderRowComponents => GetCustomRepository<IProductIn_OrderRowComponentRepository>();

        public IRowRepository Rows => GetCustomRepository<IRowRepository>();

        public ISeatRepository Seats => GetCustomRepository<ISeatRepository>();

        public ISessionRepository Sessions => GetCustomRepository<ISessionRepository>();

        public ITicketRepository Tickets => GetCustomRepository<ITicketRepository>();

        public IRepository<DiscountType> DiscountTypes => GetEntityRepository<DiscountType>();

        public IRepository<Campaign> Campaigns => GetEntityRepository<Campaign>();

        public IRepository<Cinema> Cinemas => GetEntityRepository<Cinema>();

        public IRepository<Currency> Currencies => GetEntityRepository<Currency>();

        public IRepository<InfoType> InfoTypes => GetEntityRepository<InfoType>();

        public IRepository<Movie> Movies => GetEntityRepository<Movie>();

        public IRepository<ProductGroup> ProductGroups => GetEntityRepository<ProductGroup>();

        public IRepository<UserGroup> UserGroups => GetEntityRepository<UserGroup>();

        public void SaveChanges()
        {
            _applicationDbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _applicationDbContext.SaveChangesAsync();
        }

        public IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class
        {
            return _repositoryProvider.GetEntityRepository<TEntity>();
        }

        public TRepositoryInterface GetCustomRepository<TRepositoryInterface>() where TRepositoryInterface : class
        {
            return _repositoryProvider.GetCustomRepository<TRepositoryInterface>();
        }



    }
}
