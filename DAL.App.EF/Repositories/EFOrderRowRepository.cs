﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFOrderRowRepository : EFRepository<OrderRow>, IOrderRowRepository
    {
        public EFOrderRowRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<OrderRow> All()
        {
            return RepositoryDbSet.Include(o => o.Order);
        }

        public override async Task<IEnumerable<OrderRow>> AllAsync()
        {
            return await RepositoryDbSet.Include(o => o.Order).ToListAsync();
        }

        public override async Task<OrderRow> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(o => o.Order)
                .SingleOrDefaultAsync(m => m.OrderRowId == (int)id[0]);
        }
    }
}
