﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFOrderRepository : EFRepository<Order>, IOrderRepository
    {
        public EFOrderRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Order> All()
        {
            return RepositoryDbSet.Include(o => o.User);
        }

        public override async Task<IEnumerable<Order>> AllAsync()
        {
            return await RepositoryDbSet.Include(o => o.User).ToListAsync();
        }

        public override async Task<Order> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(o => o.User)
                .SingleOrDefaultAsync(m => m.OrderId == (int)id[0]);
        }
    }
}
