﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFProductIn_OrderRowComponent_Repository : EFRepository<ProductIn_OrderRowComponent>, IProductIn_OrderRowComponentRepository
    {
        public EFProductIn_OrderRowComponent_Repository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<ProductIn_OrderRowComponent> All()
        {
            return RepositoryDbSet.Include(p => p.OrderRowComponent).Include(p => p.Product);
        }

        public override async Task<IEnumerable<ProductIn_OrderRowComponent>> AllAsync()
        {
            return await RepositoryDbSet.Include(p => p.OrderRowComponent).Include(p => p.Product).ToListAsync();
        }

        public override async Task<ProductIn_OrderRowComponent> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(p => p.OrderRowComponent)
                .Include(p => p.Product)
                .SingleOrDefaultAsync(m => m.ProductIn_OrderRowComponentId == (int)id[0]);
        }

    }
}
