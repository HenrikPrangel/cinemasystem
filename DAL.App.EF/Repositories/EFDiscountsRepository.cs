﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFDiscountsRepository : EFRepository<Discount>, IDiscountRepository
    {
        public EFDiscountsRepository(DbContext dataContext) : base(dataContext)
        {
        }


        public override IEnumerable<Discount> All()
        {
            return RepositoryDbSet.Include(d => d.Campaign).Include(d => d.DiscountType).Include(d => d.Order)
                .Include(d => d.Product).Include(d => d.ProductGroup).Include(d => d.Ticket).Include(d => d.UserGroup);
        }

        public override async Task<IEnumerable<Discount>> AllAsync()
        {
            return await RepositoryDbSet.Include(d => d.Campaign).Include(d => d.DiscountType).Include(d => d.Order)
                .Include(d => d.Product).Include(d => d.ProductGroup).Include(d => d.Ticket).Include(d => d.UserGroup).ToListAsync();
        }


        public override async Task<Discount> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(d => d.Campaign)
                .Include(d => d.DiscountType)
                .Include(d => d.Order)
                .Include(d => d.Product)
                .Include(d => d.ProductGroup)
                .Include(d => d.Ticket)
                .Include(d => d.UserGroup)
                .SingleOrDefaultAsync(m => m.DiscountId == (int)id[0]);
        }


    }
}
