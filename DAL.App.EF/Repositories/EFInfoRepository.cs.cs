﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFInfoRepository : EFRepository<Info>, IInfoRepository
    {
        public EFInfoRepository(DbContext dataContext) : base(dataContext)
        {
        }
        //TODO: .Includes should be improved to be more efficient
        public override IEnumerable<Info> All()
        {
            return RepositoryDbSet.Include(i => i.Campaign).Include(i => i.Cinema).Include(i => i.Discount).Include(i => i.DiscountType)
                .Include(i => i.InfoType).Include(i => i.Movie).Include(i => i.Product).Include(i => i.UserGroup);
        }

        public override async Task<IEnumerable<Info>> AllAsync()
        {
            return await RepositoryDbSet.Include(i => i.Campaign).Include(i => i.Cinema).Include(i => i.Discount).Include(i => i.DiscountType)
                .Include(i => i.InfoType).Include(i => i.Movie).Include(i => i.Product).Include(i => i.UserGroup).ToListAsync();
        }

        public override async Task<Info> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(i => i.Campaign)
                .Include(i => i.Cinema)
                .Include(i => i.Discount)
                .Include(i => i.DiscountType)
                .Include(i => i.InfoType)
                .Include(i => i.Movie)
                .Include(i => i.Product)
                .Include(i => i.UserGroup)
                .SingleOrDefaultAsync(m => m.InfoId == (int)id[0]);
        }

    }
}
