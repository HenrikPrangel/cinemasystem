﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFPricesRepository : EFRepository<Price>, IPriceRepository
    {
        public EFPricesRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Price> All()
        {
            return RepositoryDbSet.Include(p => p.Campaign).Include(p => p.Currency).Include(p => p.Product).Include(p => p.Ticket);
        }

        public override async Task<IEnumerable<Price>> AllAsync()
        {
            return await RepositoryDbSet.Include(p => p.Campaign).Include(p => p.Currency).Include(p => p.Product).Include(p => p.Ticket).ToListAsync();
        }

        public override async Task<Price> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(p => p.Campaign)
                .Include(p => p.Currency)
                .Include(p => p.Product)
                .Include(p => p.Ticket)
                .SingleOrDefaultAsync(m => m.PriceId == (int)id[0]);
        }
    }
}
