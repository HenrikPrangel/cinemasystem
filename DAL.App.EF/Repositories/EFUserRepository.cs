﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFUserRepository : EFRepository<User>, IUserRepository
    {
        public EFUserRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<User> All()
        {
            return RepositoryDbSet.Include(u => u.UserGroup);
        }

        public override async Task<IEnumerable<User>> AllAsync()
        {
            return await RepositoryDbSet.Include(u => u.UserGroup).ToListAsync();
        }

        public override async Task<User> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(u => u.UserGroup)
                .SingleOrDefaultAsync(m => m.UserId == (int)id[0]);
        }
        //public bool Exists(int id)
        //{
        //    return RepositoryDbSet.Any(e => e.UserId == id);
        //}
    }
}
