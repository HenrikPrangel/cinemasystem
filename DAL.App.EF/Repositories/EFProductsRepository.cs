﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFProductsRepository : EFRepository<Product>, IProductRepository
    {
        public EFProductsRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Product> All()
        {
            return RepositoryDbSet.Include(p => p.ProductGroup);
        }

        public override async Task<IEnumerable<Product>> AllAsync()
        {
            return await RepositoryDbSet.Include(p => p.ProductGroup).ToListAsync();
        }

        public override async Task<Product> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(p => p.ProductGroup)
                .SingleOrDefaultAsync(m => m.ProductId == (int)id[0]);
        }
    }
}
