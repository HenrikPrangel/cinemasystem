﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFMovieInCinemaRepository : EFRepository<MovieInCinema>, IMovieInCinemaRepository
    {
        public EFMovieInCinemaRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<MovieInCinema> All()
        {
            return RepositoryDbSet.Include(m => m.Cinema).Include(m => m.Movie);
        }

        public override async Task<IEnumerable<MovieInCinema>> AllAsync()
        {
            return await RepositoryDbSet.Include(m => m.Cinema).Include(m => m.Movie).ToListAsync();
        }

        public override async Task<MovieInCinema> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(m => m.Cinema)
                .Include(m => m.Movie)
                .SingleOrDefaultAsync(m => m.MovieInCinemaId == (int)id[0]);
        }
    }
}
