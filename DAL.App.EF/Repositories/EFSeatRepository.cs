﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFSeatRepository : EFRepository<Seat>, ISeatRepository
    {
        public EFSeatRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Seat> All()
        {
            return RepositoryDbSet.Include(s => s.Row);
        }

        public override async Task<IEnumerable<Seat>> AllAsync()
        {
            return await RepositoryDbSet.Include(s => s.Row).ToListAsync();
        }

        public override async Task<Seat> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(s => s.Row)
                .SingleOrDefaultAsync(m => m.SeatId == (int)id[0]);
        }
    }
}
