﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFPaymentsRepository : EFRepository<Payment>, IPaymentRepository
    {
        public EFPaymentsRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Payment> All()
        {
            return RepositoryDbSet.Include(p => p.Order);
        }

        public override async Task<IEnumerable<Payment>> AllAsync()
        {
            return await RepositoryDbSet.Include(p => p.Order).ToListAsync();
        }

        public override async Task<Payment> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(p => p.Order)
                .SingleOrDefaultAsync(m => m.PaymentId == (int)id[0]);
        }
    }
}
