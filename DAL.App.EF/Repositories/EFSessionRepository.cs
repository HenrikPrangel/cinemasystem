﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFSessionRepository : EFRepository<Session>, ISessionRepository
    {
        public EFSessionRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Session> All()
        {
            return RepositoryDbSet.Include(s => s.CinemaHall).Include(s => s.MovieInCinema);
        }

        public override async Task<IEnumerable<Session>> AllAsync()
        {
            return await RepositoryDbSet.Include(s => s.CinemaHall).Include(s => s.MovieInCinema).ToListAsync();
        }

        public override async Task<Session> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(s => s.CinemaHall)
                .Include(s => s.MovieInCinema)
                .SingleOrDefaultAsync(m => m.SessionId == (int)id[0]);
        }

    }
}
