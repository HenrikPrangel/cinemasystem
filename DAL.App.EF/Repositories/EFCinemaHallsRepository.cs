﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class EFCinemaHallsRepository : EFRepository<CinemaHall>, ICinemaHallRepository
    {
        public EFCinemaHallsRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<CinemaHall> All()
        {
            return RepositoryDbSet.Include(c => c.Cinema);
        }

        public override async Task<IEnumerable<CinemaHall>> AllAsync()
        {
            return await RepositoryDbSet.Include(c => c.Cinema).ToListAsync();
        }

        public override async Task<CinemaHall> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(c => c.Cinema)
                .SingleOrDefaultAsync(m => m.CinemaHallId == (int)id[0]);
        }

    }
}
