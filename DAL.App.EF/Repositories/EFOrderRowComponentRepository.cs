﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFOrderRowComponentRepository : EFRepository<OrderRowComponent>, IOrderRowComponentRepository
    {
        public EFOrderRowComponentRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<OrderRowComponent> All()
        {
            return RepositoryDbSet.Include(o => o.OrderRow);
        }

        public override async Task<IEnumerable<OrderRowComponent>> AllAsync()
        {
            return await RepositoryDbSet.Include(o => o.OrderRow).ToListAsync();
        }

        public override async Task<OrderRowComponent> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(o => o.OrderRow)
                .SingleOrDefaultAsync(m => m.OrderRowComponentId == (int)id[0]);
        }
    }
}
