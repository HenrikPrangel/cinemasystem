﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFTicketRepository : EFRepository<Ticket>, ITicketRepository
    {
        public EFTicketRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Ticket> All()
        {
            return RepositoryDbSet.Include(t => t.OrderRow).Include(t => t.Seat).Include(t => t.Session);
        }

        public override async Task<IEnumerable<Ticket>> AllAsync()
        {
            return await RepositoryDbSet.Include(t => t.OrderRow).Include(t => t.Seat).Include(t => t.Session).ToListAsync();

        }

        public override async Task<Ticket> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(t => t.OrderRow)
                .Include(t => t.Seat)
                .Include(t => t.Session)
                .SingleOrDefaultAsync(m => m.TicketId == (int)id[0]);
        }
    }
}
