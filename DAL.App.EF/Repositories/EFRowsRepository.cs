﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFRowsRepository : EFRepository<Row>, IRowRepository
    {
        public EFRowsRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public override IEnumerable<Row> All()
        {
            return RepositoryDbSet.Include(r => r.CinemaHall);
        }

        public override async Task<IEnumerable<Row>> AllAsync()
        {
            return await RepositoryDbSet.Include(r => r.CinemaHall).ToListAsync();
        }

        public override async Task<Row> FindAsync(params object[] id)
        {
            return await RepositoryDbSet
                .Include(r => r.CinemaHall)
                .SingleOrDefaultAsync(m => m.RowId == (int)id[0]);
        }
    }
}
