﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces.Helpers;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryFactory : IRepositoryFactory
    {
        private readonly Dictionary<Type, Func<IDataContext, object>> _customRepositoryFactories
            = GetCustomRepoFactories();

        private static Dictionary<Type, Func<IDataContext, object>> GetCustomRepoFactories()
        {
            return new Dictionary<Type, Func<IDataContext, object>>()
            {
                {typeof(IUserRepository), (dataContext) => new EFUserRepository(dataContext as ApplicationDbContext) },
                {typeof(ICinemaHallRepository), (dataContext) => new EFCinemaHallsRepository(dataContext as ApplicationDbContext) },
                {typeof(IDiscountRepository), (dataContext) => new EFDiscountsRepository(dataContext as ApplicationDbContext) },
                {typeof(IInfoRepository), (dataContext) => new EFInfoRepository(dataContext as ApplicationDbContext) },
                {typeof(IMovieInCinemaRepository), (dataContext) => new EFMovieInCinemaRepository(dataContext as ApplicationDbContext) },
                {typeof(IOrderRepository), (dataContext) => new EFOrderRepository(dataContext as ApplicationDbContext) },
                {typeof(IOrderRowRepository), (dataContext) => new EFOrderRowRepository(dataContext as ApplicationDbContext) },
                {typeof(IOrderRowComponentRepository), (dataContext) => new EFOrderRowComponentRepository(dataContext as ApplicationDbContext) },
                {typeof(IPaymentRepository), (dataContext) => new EFPaymentsRepository(dataContext as ApplicationDbContext) },
                {typeof(IPriceRepository), (dataContext) => new EFPricesRepository(dataContext as ApplicationDbContext) },
                {typeof(IProductRepository), (dataContext) => new EFProductsRepository(dataContext as ApplicationDbContext) },
                {typeof(IProductIn_OrderRowComponentRepository), (dataContext) => new EFProductIn_OrderRowComponent_Repository(dataContext as ApplicationDbContext) },
                {typeof(IRowRepository), (dataContext) => new EFRowsRepository(dataContext as ApplicationDbContext) },
                {typeof(ISeatRepository), (dataContext) => new EFSeatRepository(dataContext as ApplicationDbContext) },
                {typeof(ISessionRepository), (dataContext) => new EFSessionRepository(dataContext as ApplicationDbContext) },
                {typeof(ITicketRepository), (dataContext) => new EFTicketRepository(dataContext as ApplicationDbContext) },
            };
        }

        public Func<IDataContext, object> GetCustomRepositoryFactory<TRepoInterface>() where TRepoInterface : class
        {
            _customRepositoryFactories.TryGetValue(
                typeof(TRepoInterface), 
                out Func<IDataContext, object> factory
            );
            return factory;
        }

        public Func<IDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {

            return (dataContext) => new EFRepository<TEntity>(dataContext as ApplicationDbContext);
        }
    }
}
